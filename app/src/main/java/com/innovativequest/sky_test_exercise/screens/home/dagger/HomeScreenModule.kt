package com.innovativequest.sky_test_exercise.screens.home.dagger

import com.innovativequest.sky_test_exercise.networkservices.DataService
import com.innovativequest.sky_test_exercise.screens.home.HomeScreenActivity
import com.innovativequest.sky_test_exercise.screens.home.mvp.DefaultHomeScreenView
import com.innovativequest.sky_test_exercise.screens.home.mvp.HomeScreenModel
import com.innovativequest.sky_test_exercise.screens.home.mvp.HomeScreenPresenter
import com.innovativequest.sky_test_exercise.screens.home.mvp.HomeScreenView
import com.innovativequest.sky_test_exercise.utils.PreferencesManager
import com.squareup.picasso.Picasso

import dagger.Module
import dagger.Provides

/**
 * Created by Ghous on 04/10/2019.
 */
@Module
class HomeScreenModule(internal val homeScreenActivity: HomeScreenActivity) {

    @Provides
    @HomeScreenScope
    fun homeScreenView(preferencesManager: PreferencesManager, picasso: Picasso): HomeScreenView {
        return DefaultHomeScreenView(homeScreenActivity, preferencesManager, picasso)
    }

    @Provides
    @HomeScreenScope
    fun homeScreenPresenter(homeScreenView: HomeScreenView,
                            homeScreenModel: HomeScreenModel): HomeScreenPresenter {
        return HomeScreenPresenter(homeScreenView, homeScreenModel)
    }

    @Provides
    @HomeScreenScope
    fun homeScreenModel(dataService: DataService, preferencesManager: PreferencesManager): HomeScreenModel {
        return HomeScreenModel(dataService, homeScreenActivity,  preferencesManager)
    }


}
