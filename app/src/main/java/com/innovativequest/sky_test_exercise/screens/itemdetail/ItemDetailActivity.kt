package com.innovativequest.sky_test_exercise.screens.itemdetail

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.innovativequest.sky_test_exercise.app.RxMvpApp
import com.innovativequest.sky_test_exercise.models.ItemDataResponse
import com.innovativequest.sky_test_exercise.screens.itemdetail.dagger.DaggerItemDetailActivityComponent
import com.innovativequest.sky_test_exercise.screens.itemdetail.dagger.ItemDetailModule
import com.innovativequest.sky_test_exercise.screens.itemdetail.mvp.ItemDetailPresenter
import com.innovativequest.sky_test_exercise.screens.itemdetail.mvp.ItemDetailView
import javax.inject.Inject

class ItemDetailActivity : AppCompatActivity() {

    @Inject
    lateinit var itemDetailView: ItemDetailView

    @Inject
    lateinit var itemDetailPresenter: ItemDetailPresenter

    @SuppressLint("Range")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        DaggerItemDetailActivityComponent.builder()
                .rxMvpAppComponent(RxMvpApp.get(this).component())
                .itemDetailModule(ItemDetailModule(this))
                .build().inject(this)
        setContentView(itemDetailView.view)
        itemDetailPresenter.onCreate(mItemItem)

    }

    companion object {
        lateinit var mItemItem: ItemDataResponse
        fun start(context: Context, itemItem: ItemDataResponse) {
            mItemItem = itemItem
            val intent = Intent(context, ItemDetailActivity::class.java)
            context.startActivity(intent) }

    }

    override fun onDestroy() {
        super.onDestroy()
        itemDetailPresenter.onDestroy()
    }

}
