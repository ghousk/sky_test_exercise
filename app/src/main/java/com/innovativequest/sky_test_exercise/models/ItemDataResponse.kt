package com.innovativequest.sky_test_exercise.models

import com.google.gson.annotations.SerializedName

data class ItemDataResponse(

	@field:SerializedName("committer")
	val committer: Committer? = null,

	@field:SerializedName("author")
	val author: Author? = null,

	@field:SerializedName("html_url")
	val htmlUrl: String? = null,

	@field:SerializedName("commit")
	val commit: Commit? = null,

	@field:SerializedName("comments_url")
	val commentsUrl: String? = null,

	@field:SerializedName("sha")
	val sha: String? = null,

	@field:SerializedName("url")
	val url: String? = null,

	@field:SerializedName("node_id")
	val nodeId: String? = null,

	@field:SerializedName("parents")
	val parents: List<ParentsItem?>? = null
)