package com.innovativequest.sky_test_exercise.screens.itemdetail.mvp

import android.annotation.SuppressLint
import android.util.Log
import com.innovativequest.sky_test_exercise.R
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import butterknife.BindView
import butterknife.ButterKnife
import com.innovativequest.sky_test_exercise.models.ItemDataResponse
import com.innovativequest.sky_test_exercise.models.ItemDetailFirst
import com.innovativequest.sky_test_exercise.screens.itemdetail.ItemDetailActivity
import com.innovativequest.sky_test_exercise.utils.Utils
import com.jakewharton.rxbinding2.view.RxView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import io.reactivex.Observable
import java.lang.Exception

/**
 * Created by Ghous on 04/10/2019.
 */
@SuppressLint("ViewConstructor")
class DefaultItemDetailView (private val itemDetailActivity: ItemDetailActivity, private val mPicasso : Picasso) : FrameLayout(itemDetailActivity), ItemDetailView {

    @BindView(R.id.title_bk_actionbar_btn_start)
    internal lateinit var mToolbarStartButton: ImageView

    @BindView(R.id.title_bk_actionbar_btn_end)
    internal lateinit var mToolbarEndButton: ImageView

    @BindView(R.id.title_bk_actionbar_title_centre)
    internal lateinit var mToolBarTitleText: AppCompatTextView

    @BindView(R.id.item_name_tv)
    internal lateinit var mItemTitle: AppCompatTextView

    @BindView(R.id.sub_item_name_tv)
    internal lateinit var mItemArtist: AppCompatTextView

    @BindView(R.id.body_item_1_tv)
    internal lateinit var mBodyItem1Tv: AppCompatTextView

    @BindView(R.id.body_item_2_tv)
    internal lateinit var mBodyItem2Tv: AppCompatTextView

    @BindView(R.id.body_item_3_tv)
    internal lateinit var mBodyItem3Tv: AppCompatTextView

    @BindView(R.id.body_item_4_tv)
    internal lateinit var mBodyItem4Tv: AppCompatTextView

    @BindView(R.id.body_item_5_tv)
    internal lateinit var mBodyItem5Tv: AppCompatTextView

    @BindView(R.id.body_item_6_tv)
    internal lateinit var mBodyItem6Tv: AppCompatTextView

    @BindView(R.id.item_detail_iv)
    internal lateinit var mItemImageIv: AppCompatImageView

    @BindView(R.id.progress_bar)
    internal lateinit var progressBar: ProgressBar

    override val view: View
        get() = this


    init {
        //Inflate the layout into the viewgroup
        layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        View.inflate(context, R.layout.default_item_detail, this)
        ButterKnife.bind(this)
        mToolbarEndButton.visibility = GONE
    }

    override fun toolbarStartBtnObs(): Observable<Any> {
        return RxView.clicks(mToolbarStartButton)
    }

    override fun setItem(dataItem: ItemDataResponse) {
        itemDetailActivity.runOnUiThread {
            try {
                mPicasso.load(dataItem.author?.avatarUrl).placeholder(R.drawable.item_logo).into(mItemImageIv, object:
                    Callback {

                    override fun onSuccess() {
                        //set animations here
                        Log.d(javaClass.name, "Success")
                    }

                    override fun onError(e: Exception?) {
                        Log.d(javaClass.name, e?.message)
                    }
                })

                mToolBarTitleText.text      =   itemDetailActivity.getString(R.string.body_item_6, dataItem.commit?.author?.name)
                mItemTitle.text             =   dataItem.commit?.message
                mItemArtist.text            =   dataItem.commit?.author?.name
                mBodyItem1Tv.text           =   itemDetailActivity.getString(R.string.body_item_1, Utils.getDateInDisplayableFormat(dataItem.commit?.committer?.date))
                mBodyItem2Tv.text           =   itemDetailActivity.getString(R.string.body_item_2, dataItem.commit?.committer?.email)
            }
            catch (e: NullPointerException){
                Log.d(javaClass.name, e.message)
            }
        }
    }

    override fun showError(message: String) {
//        val view = itemDetailActivity.getLayoutInflater().inflate(R.layout.custom_toast_layout, null)
//        Utils.showCustomErrorToast(itemDetailActivity, message, view)
    }

}








