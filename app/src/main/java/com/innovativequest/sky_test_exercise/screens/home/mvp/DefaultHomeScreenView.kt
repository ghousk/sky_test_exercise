package com.innovativequest.sky_test_exercise.screens.home.mvp

import android.annotation.SuppressLint
import android.content.Context
import com.innovativequest.sky_test_exercise.R
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import android.widget.FrameLayout
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.innovativequest.sky_test_exercise.models.ItemDataResponse
import com.innovativequest.sky_test_exercise.screens.home.HomeScreenActivity
import com.innovativequest.sky_test_exercise.screens.home.mvp.adapter.HomeScreenItemListAdapter
import io.reactivex.Observable
import com.innovativequest.sky_test_exercise.utils.PreferencesManager
import com.squareup.picasso.Picasso

/**
 * Created by Ghous on 04/10/2019.
 */
@SuppressLint("ViewConstructor")
class DefaultHomeScreenView (private val homeScreenActivity: HomeScreenActivity, private val preferencesManager: PreferencesManager, private val mPicasso: Picasso) : FrameLayout(homeScreenActivity), HomeScreenView {

    @BindView(R.id.default_homeview_toolbar)
    internal lateinit var mToolbar: Toolbar

    @BindView(R.id.home_actionbar_title_centre)
    internal lateinit var mActionBarTitle: AppCompatTextView

    @BindView(R.id.progressBar)
    internal lateinit var progressBar: ProgressBar

    @BindView(R.id.home_item_list_recycler_view)
    internal lateinit var mItemsRecyclerView: RecyclerView

    private val mAdapter: HomeScreenItemListAdapter

    override val view: View
        get() = this


    init {
        //Inflate the layout into the viewgroup
        layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        View.inflate(context, R.layout.default_home_screen_view, this)
        ButterKnife.bind(this)

        mActionBarTitle.text = homeScreenActivity.getString(R.string.app_name)

        mAdapter = HomeScreenItemListAdapter(mPicasso)
        mItemsRecyclerView.layoutManager = LinearLayoutManager(homeScreenActivity)
        mItemsRecyclerView.adapter = mAdapter
    }

    override fun setLoading(loading: Boolean) {
        if (loading) {
            progressBar.visibility = View.VISIBLE
        } else {
            progressBar.visibility = View.GONE
        }
    }

    override fun showError(message: String) {
//        val view = homeScreenActivity.getLayoutInflater().inflate(R.layout.custom_toast_layout, null)
//        Utils.showCustomErrorToast(homeScreenActivity, message, view)
    }

    override fun listItemClicks(): Observable<ItemDataResponse> {
        return mAdapter.clickEvent
    }

    override fun setListItems(itemList: List<ItemDataResponse>?) {
        if(itemList!=null && itemList.isNotEmpty()){
            mAdapter.setData(itemList)
        }
    }

    override fun hideKeyBoard() {
        (homeScreenActivity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                .hideSoftInputFromWindow(view.windowToken, 0)
    }

}








