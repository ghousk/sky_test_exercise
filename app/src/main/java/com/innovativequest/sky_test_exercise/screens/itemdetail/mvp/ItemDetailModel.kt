package com.innovativequest.sky_test_exercise.screens.itemdetail.mvp

import com.innovativequest.sky_test_exercise.models.ItemDataResponse
import com.innovativequest.sky_test_exercise.models.ItemDetailSecond
import com.innovativequest.sky_test_exercise.models.ItemDetailFirst
import com.innovativequest.sky_test_exercise.networkservices.DataService
import io.reactivex.Observable
import kotlin.collections.ArrayList

/**
 * Created by Ghous on 04/10/2019.
 */
class ItemDetailModel(private val mDataService: DataService) {

    fun getItemDetailFirst() : Observable<ArrayList<ItemDetailFirst>> {
        return mDataService.itemDetailFirst
    }

//    fun getItemDetailSecond() : Observable<ArrayList<ItemDetailSecond>>{
//        return mDataService.itemDetailSecond
//    }
}


