package com.innovativequest.sky_test_exercise.app.builder;

import android.content.Context;

import com.innovativequest.sky_test_exercise.networkservices.DataService;
import com.innovativequest.sky_test_exercise.utils.EnvironmentParameters;
import com.innovativequest.sky_test_exercise.utils.PreferencesManager;
import com.squareup.picasso.Picasso;
import com.twistedequations.rx2.AndroidRxSchedulers;

import dagger.Component;

@AppScope
@Component(modules = {RxMvpAppModule.class, NetworkModule.class, RestServiceModule.class, GsonModule.class, RxModule.class})
public interface RxMvpAppComponent {

    Context context();

    Picasso picasso();

    EnvironmentParameters environmentparameters();

    PreferencesManager preferencesmanager();

    AndroidRxSchedulers rxSchedulers();

    DataService dataService();

}
