package com.innovativequest.sky_test_exercise.screens.home.dagger

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

import javax.inject.Scope

/**
 * Created by Ghous on 04/10/2019.
 */
@Scope
@Retention(RetentionPolicy.CLASS)
annotation class HomeScreenScope
