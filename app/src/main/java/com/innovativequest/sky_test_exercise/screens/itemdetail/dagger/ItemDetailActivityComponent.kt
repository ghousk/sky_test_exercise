package com.innovativequest.sky_test_exercise.screens.itemdetail.dagger

import com.innovativequest.sky_test_exercise.app.builder.RxMvpAppComponent
import com.innovativequest.sky_test_exercise.screens.itemdetail.ItemDetailActivity
import dagger.Component

/**
 * Created by Ghous on 04/10/2019.
 */
@ItemDetailScope
@Component(modules = arrayOf(ItemDetailModule::class), dependencies = arrayOf(RxMvpAppComponent::class))
interface ItemDetailActivityComponent {
    fun inject(itemDetailActivity: ItemDetailActivity)
}