package com.innovativequest.sky_test_exercise.screens.itemdetail.mvp

import android.util.Log
import com.innovativequest.sky_test_exercise.models.ItemDataResponse
import com.innovativequest.sky_test_exercise.screens.itemdetail.ItemDetailActivity
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import com.innovativequest.sky_test_exercise.utils.PreferencesManager
import io.reactivex.android.schedulers.AndroidSchedulers


/**
 * Created by Ghous on 04/10/2019.
 */
class ItemDetailPresenter (private val activityItem: ItemDetailActivity, private val itemDetailView: ItemDetailView, private  val itemDetailModel: ItemDetailModel,
                           private val preferencesManager: PreferencesManager) {

    private val compositeDisposable = CompositeDisposable()

    fun onCreate(dataItem: ItemDataResponse) {

        compositeDisposable.addAll(
                subscribeToBackButton())

        itemDetailView.setItem(dataItem)
    }

    private fun subscribeToBackButton(): Disposable {
        return itemDetailView.toolbarStartBtnObs().subscribe {
            activityItem.onBackPressed()
        }
    }

    fun onDestroy() {
        compositeDisposable.clear()
    }

}