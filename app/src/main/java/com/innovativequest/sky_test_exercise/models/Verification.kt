package com.innovativequest.sky_test_exercise.models

import com.google.gson.annotations.SerializedName

data class Verification(

	@field:SerializedName("reason")
	val reason: String? = null,

	@field:SerializedName("signature")
	val signature: Any? = null,

	@field:SerializedName("payload")
	val payload: Any? = null,

	@field:SerializedName("verified")
	val verified: Boolean? = null
)