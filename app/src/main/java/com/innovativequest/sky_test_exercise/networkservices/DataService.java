package com.innovativequest.sky_test_exercise.networkservices;

import com.innovativequest.sky_test_exercise.models.ItemDataResponse;
import com.innovativequest.sky_test_exercise.models.ItemDetailSecond;
import com.innovativequest.sky_test_exercise.models.ItemDetailFirst;
import com.innovativequest.sky_test_exercise.utils.AppConstants;

import java.util.ArrayList;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Ghous on 04/10/2019.
 */


public interface DataService {
    //************************************************
    //HEAD REQUESTS
    //************************************************

    //************************************************
    // GET REQUESTS
    //************************************************
    @GET(AppConstants.GET_POSTS)
    Observable<ArrayList<ItemDataResponse>> getDataItems();

    @GET(AppConstants.GET_USERS)
    Observable<ArrayList<ItemDetailFirst>> getItemDetailFirst();

    @GET(AppConstants.GET_COMMENTS)
    Observable<ArrayList<ItemDetailSecond>> getItemDetailSecond();

    //************************************************
    // POST REQUESTS
    //************************************************


}