package com.innovativequest.sky_test_exercise.screens.home.dagger

import com.innovativequest.sky_test_exercise.app.builder.RxMvpAppComponent
import com.innovativequest.sky_test_exercise.screens.home.HomeScreenActivity
import dagger.Component

/**
 * Created by Ghous on 04/10/2019.
 */
@HomeScreenScope
@Component(modules = arrayOf(HomeScreenModule::class), dependencies = arrayOf(RxMvpAppComponent::class))
interface HomeScreenActivityComponent {
    fun inject(homeScreenActivity: HomeScreenActivity)
}