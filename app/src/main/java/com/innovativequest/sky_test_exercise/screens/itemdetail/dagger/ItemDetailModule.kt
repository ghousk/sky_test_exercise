package com.innovativequest.sky_test_exercise.screens.itemdetail.dagger

import com.innovativequest.sky_test_exercise.networkservices.DataService
import com.innovativequest.sky_test_exercise.screens.itemdetail.ItemDetailActivity
import com.innovativequest.sky_test_exercise.screens.itemdetail.mvp.DefaultItemDetailView
import com.innovativequest.sky_test_exercise.screens.itemdetail.mvp.ItemDetailModel
import com.innovativequest.sky_test_exercise.screens.itemdetail.mvp.ItemDetailPresenter
import com.innovativequest.sky_test_exercise.screens.itemdetail.mvp.ItemDetailView
import com.innovativequest.sky_test_exercise.utils.PreferencesManager
import com.squareup.picasso.Picasso

import dagger.Module
import dagger.Provides

/**
 * Created by Ghous on 04/10/2019.
 */
@Module
class ItemDetailModule(internal val itemDetailActivity: ItemDetailActivity) {

    @Provides
    @ItemDetailScope
    fun itemDetailView(picasso: Picasso): ItemDetailView {
        return DefaultItemDetailView(itemDetailActivity, picasso)
    }

    @Provides
    @ItemDetailScope
    fun itemDetailPresenter(itemDetailView: ItemDetailView,
                            itemDetailModel: ItemDetailModel,
                            preferencesManager: PreferencesManager): ItemDetailPresenter {
        return ItemDetailPresenter(itemDetailActivity, itemDetailView, itemDetailModel, preferencesManager)
    }

    @Provides
    @ItemDetailScope
    fun itemDetailModel(dataService: DataService): ItemDetailModel {
        return ItemDetailModel(dataService)
    }


}
