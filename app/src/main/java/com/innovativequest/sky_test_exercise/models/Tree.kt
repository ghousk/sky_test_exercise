package com.innovativequest.sky_test_exercise.models

import com.google.gson.annotations.SerializedName

data class Tree(

	@field:SerializedName("sha")
	val sha: String? = null,

	@field:SerializedName("url")
	val url: String? = null
)